/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.censoweb.persistence;

import java.util.List;
import co.edu.sena.censoweb.model.Encuestador;
import javax.ejb.Local;

/**
 * Fecha: 12/07/2022
 * @author Aprendiz
 * Objetivo: Interface DAO Encuestador
 */
@Local
public interface IEncuestadorDAO {
    public void insert(Encuestador encuestador)throws Exception;
    public void update(Encuestador encuestador)throws Exception;
    public void delete(Encuestador encuestador)throws Exception;
    
    public Encuestador find(Long cedula)throws Exception;
    public List<Encuestador> findAll() throws Exception;
}
