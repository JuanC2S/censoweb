/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.censoweb.persistence;

import java.util.List;
import javax.persistence.Query;
import co.edu.sena.censoweb.model.Encuesta;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Aprendiz
 */
@Stateless

public class EncuestaDAO implements IEncuestaDAO{
    @PersistenceContext
    private EntityManager entityManager;
    
    @Override
    public void insert(Encuesta encuesta) throws Exception {
        try {
            entityManager.persist(encuesta);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public void update(Encuesta encuesta) throws Exception {
        try {
            entityManager.merge(encuesta);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public void delete(Encuesta encuesta) throws Exception {
        try {
            entityManager.remove(encuesta);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public Encuesta find(Integer numeroFormulario) throws Exception {
        try {
            return entityManager.find(Encuesta.class, numeroFormulario);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public List<Encuesta> findAll() throws Exception {
        try {
            Query query = entityManager.createNamedQuery("Encuesta.findAll");
            return query.getResultList();
        } catch (Exception e) {
            throw e;
        }
    }
    
}
