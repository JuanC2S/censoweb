/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.censoweb.persistence;

import co.edu.sena.censoweb.model.Encuesta;
import java.util.List;
import javax.ejb.Local;


/**
 * Fecha: 12/07/2022
 * @author Aprendiz
 * Objetivo: Interface DAO Encuesta
 */
@Local
public interface IEncuestaDAO {
    public void insert(Encuesta encuesta)throws Exception;
    public void update(Encuesta encuesta)throws Exception;
    public void delete(Encuesta encuesta)throws Exception;
    
    public Encuesta find(Integer numeroFormulario)throws Exception;
    public List<Encuesta> findAll() throws Exception;
}
