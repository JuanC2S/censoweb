/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.censoweb.persistence;

import java.util.List;
import co.edu.sena.censoweb.model.UsoComercial;
import javax.ejb.Local;

/**
 * Fecha: 12/07/2022
 * @author Aprendiz
 * Objetivo: Interface DAO Uso_comercial
 */
@Local
public interface IUsoComercialDAO {
    public void insert(UsoComercial usoComercial)throws Exception;
    public void update(UsoComercial usoComercial)throws Exception;
    public void delete(UsoComercial usoComercial)throws Exception;
    
    public UsoComercial find(Integer idUso)throws Exception;
    public List<UsoComercial> findAll() throws Exception;
}
