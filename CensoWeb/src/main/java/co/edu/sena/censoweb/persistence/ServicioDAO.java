/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.censoweb.persistence;

import java.util.List;
import javax.persistence.Query;
import co.edu.sena.censoweb.model.Servicio;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Aprendiz
 */
@Stateless
public class ServicioDAO implements IServicioDAO{
    @PersistenceContext
    private EntityManager entityManager;
    @Override
    public void insert(Servicio servicio) throws Exception {
        try {
            entityManager.persist(servicio);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public void update(Servicio servicio) throws Exception {
        try {
            entityManager.merge(servicio);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public void delete(Servicio servicio) throws Exception {
        try {
            entityManager.remove(servicio);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public Servicio find(Integer idServicio) throws Exception {
        try {
            return entityManager.find(Servicio.class, idServicio);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public List<Servicio> findAll() throws Exception {
        try {
            Query query = entityManager.createNamedQuery("Servicio.findAll");
            return query.getResultList();
        } catch (Exception e) {
            throw e;
        }
    }

}
