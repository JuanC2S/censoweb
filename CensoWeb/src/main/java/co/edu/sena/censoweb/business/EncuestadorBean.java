/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/J2EE/EJB30/StatelessEjbClass.java to edit this template
 */
package co.edu.sena.censoweb.business;

import co.edu.sena.censoweb.model.Encuestador;
import co.edu.sena.censoweb.persistence.EncuestadorDAO;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author juanc
 */
@Stateless
public class EncuestadorBean implements EncuestadorBeanLocal {
    @EJB
    private EncuestadorDAO encuestadorDAO;
    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    public void validate (Encuestador encuestador) throws Exception{
        if(encuestador == null){
            throw new Exception("El encuestador en nulo");
        }
        if(encuestador.getCedula() == 0){
            throw new Exception("La cedula es obligatoria");
        }
        if(encuestador.getNombre().isEmpty()){
            throw new Exception("El nombre es obligatorio");
        }
    }
    
    public void insert (Encuestador encuestador) throws Exception{
        validate(encuestador);
        Encuestador oldEncuestador = encuestadorDAO.find(encuestador.getCedula());
        if(oldEncuestador != null){
            throw new Exception("Ya existe encuertador  con la misma cédula");
        }
        encuestadorDAO.insert(encuestador);
    }
    
        public void update (Encuestador encuestador) throws Exception{
        validate(encuestador);
        Encuestador oldEncuestador = encuestadorDAO.find(encuestador.getCedula());
        if(oldEncuestador == null){
            throw new Exception("No existe encuertador  con esa cédula");
        }
        
        //merge: La PK no se le hace al merge
        oldEncuestador.setNombre(encuestador.getNombre());
        oldEncuestador.setTelefono(encuestador.getTelefono());
        encuestadorDAO.update(oldEncuestador);
    }
        
        public void delete (Encuestador encuestador) throws Exception{
        if(encuestador.getCedula()==0){
            throw new Exception("La cédula es obligatoria");
        }
        Encuestador oldEncuestador = encuestadorDAO.find(encuestador.getCedula());
        if(oldEncuestador == null){
            throw new Exception("No existe encuertador  con esa cédula");
        }
        encuestadorDAO.delete(oldEncuestador);
    }
        
        public Encuestador find(Long cedula) throws Exception{
            if(cedula == 0){
                throw new Exception("La cédula es obligatoria");
            }
            return encuestadorDAO.find(cedula);
        }
        
        public List<Encuestador> findAll() throws Exception{
            return encuestadorDAO.findAll();
        }
}
