/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/J2EE/EJB30/StatelessEjbClass.java to edit this template
 */
package co.edu.sena.censoweb.business;

import co.edu.sena.censoweb.model.Encuesta;
import co.edu.sena.censoweb.persistence.IEncuestaDAO;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author juanc
 */
@Stateless
public class EncuestaBean implements EncuestaBeanLocal {

    @EJB
    private IEncuestaDAO encuestaDAO;
    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")

     public void validate (Encuesta encuesta) throws Exception{
        if(encuesta == null){
            throw new Exception ("La encuesta es obligatoria");
        }
        if(encuesta.getFecha() == null){
            throw new Exception ("La fecha es obligatoria");
        }
        if(encuesta.getIdEncuestador() == null){
            throw new Exception ("El encuestador es obligatorio");
        }
        if(encuesta.getIdPredio() ==  null){
            throw new Exception ("El predio es oblidatorio");
        }
        if(encuesta.getIdServicio()==  null){
            throw new Exception ("El servicio es oblidatorio");
        }
        if(encuesta.getIdSuscriptor()==  null){
            throw new Exception ("El suscriptor es oblidatorio");
        }        
    }
    
    public void insert (Encuesta encuesta) throws Exception{
        validate(encuesta);
       encuestaDAO.insert(encuesta);
    }
    
        public void update (Encuesta encuesta) throws Exception{
        validate(encuesta);
        if(encuesta.getNumeroFormulario() == 0){
            throw  new Exception("El numero de formulario es obligatorio");
        }
        Encuesta oldEncuesta = encuestaDAO.find(encuesta.getNumeroFormulario());
        if(oldEncuesta == null){
            throw new Exception ("No existe la encuesta a modificar");
        }
        
        //merge
        oldEncuesta.setFecha(encuesta.getFecha());
        oldEncuesta.setIdEncuestador(encuesta.getIdEncuestador());
        oldEncuesta.setIdPredio(encuesta.getIdPredio());
        oldEncuesta.setIdServicio(encuesta.getIdServicio());
        oldEncuesta.setIdSuscriptor(encuesta.getIdSuscriptor());
        encuestaDAO.update(encuesta);
    }
    
    public void  delete (Encuesta encuesta) throws Exception{
       if(encuesta.getNumeroFormulario() == 0){
            throw  new Exception("El numero de formulario es obligatorio");
        }
        Encuesta oldEncuesta = encuestaDAO.find(encuesta.getNumeroFormulario());
        if(oldEncuesta == null){
            throw new Exception ("No existe la encuesta a modificar");
        }
        encuestaDAO.delete(oldEncuesta);
    }
    
    public Encuesta find(Integer numeroFormulario) throws Exception{
        if(numeroFormulario == 0){
            throw new Exception ("El numero de formulario es obligatorio");
        }
        return encuestaDAO.find(numeroFormulario);
    }
    
    public List<Encuesta> findAll() throws Exception{
        return encuestaDAO.findAll();
    }
}
