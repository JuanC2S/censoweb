/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.censoweb.persistence;

import java.util.List;
import co.edu.sena.censoweb.model.Servicio;
import javax.ejb.Local;

/**
 * Fecha: 12/07/2022
 * @author Aprendiz
 * Objetivo: Interface DAO Encuesta
 */
@Local
public interface IServicioDAO {
    public void insert(Servicio servicio)throws Exception;
    public void update(Servicio servicio)throws Exception;
    public void delete(Servicio servicio)throws Exception;
    
    public Servicio find(Integer idServicio)throws Exception;
    public List<Servicio> findAll() throws Exception;
}
