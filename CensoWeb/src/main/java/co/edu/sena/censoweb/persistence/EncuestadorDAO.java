/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.censoweb.persistence;

import java.util.List;
import javax.persistence.Query;
import co.edu.sena.censoweb.model.Encuestador;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


/**
 * Fecha:12/07/2022
 * @author Aprendiz
 * Objetivo: Clase que implementa DAO de Encuestador
 */

@Stateless
public class EncuestadorDAO implements IEncuestadorDAO{
@PersistenceContext
    private EntityManager entityManager;
    @Override
    public void insert(Encuestador encuestador) throws Exception {
        try {
            entityManager.persist(encuestador);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public void update(Encuestador encuestador) throws Exception {
        try {
            entityManager.merge(encuestador);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public void delete(Encuestador encuestador) throws Exception {
        try {
            entityManager.remove(encuestador);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public Encuestador find(Long cedula) throws Exception {
        try {
            return entityManager.find(Encuestador.class, cedula);
        } catch (RuntimeException e) {
            throw e;
        }
    }

    @Override
    public List<Encuestador> findAll() throws Exception {
        try {
            Query query = entityManager
                    .createQuery("select e from Encuestador e"); //jpql
            return query.getResultList();
        } catch (RuntimeException e) {
            throw e;
        }
    }
    
}
