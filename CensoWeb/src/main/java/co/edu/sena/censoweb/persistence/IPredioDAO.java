/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.sena.censoweb.persistence;

import java.util.List;
import co.edu.sena.censoweb.model.Predio;
import javax.ejb.Local;

/**
 * Fecha: 12/07/2022
 * @author Aprendiz
 * Objetivo: Interface DAO Encuesta
 */
@Local
public interface IPredioDAO {
    public void insert(Predio predio)throws Exception;
    public void update(Predio predio)throws Exception;
    public void delete(Predio predio)throws Exception;
    
    public Predio find(Integer idPredio)throws Exception;
    public List<Predio> findAll() throws Exception;
}
